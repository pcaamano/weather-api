# Weather API

API that consults Open Weather Map by REST, map response and return java object with information.

## ChangeLog - Version 0.0.1

- **Added**
    + Spring Boot Framework 1.5.21
    + Spring web starter dependency
    + Rest Template
    + Sl4j Logger


## Endpoints

### - Get weather for specific city

`{HOST}:{PORT}/api/weather/today/{city}/{country-code}`

Example: _http://localhost:8080/api/weather/today/London/UK_


## Responses

### Ok response:
```json
{
    "metaData": {
        "method": "GET",
        "operation": "/api/weather/today/London/UK"
    },
    "data": [
        {
            "city": "London,UK",
            "temperature": "17.79°C",
            "description": "light rain",
            "winds": "5.1 km/hs",
            "humidity": "56%"
        }
    ],
    "errors": []
}
```

### Error Response
```json
{
    "metaData": {
        "method": "GET",
        "operation": "/api/weather/today/London/UK"
    },
    "data": [],
    "errors": [
        {
            "code": "ERR-500",
            "status": "500",
            "description": "Interal server error."
        }
    ]
}
```

## Status code
- 200: OK
- 400: Bad Request
- 500: Internal Server Error
