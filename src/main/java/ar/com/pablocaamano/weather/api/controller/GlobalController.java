package ar.com.pablocaamano.weather.api.controller;

import ar.com.pablocaamano.weather.api.rest.Error;
import ar.com.pablocaamano.weather.api.rest.Data;
import ar.com.pablocaamano.weather.api.rest.Response;
import ar.com.pablocaamano.weather.api.service.WeatherService;
import ar.com.pablocaamano.weather.api.utils.ErrorBuilder;
import ar.com.pablocaamano.weather.api.utils.ResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/weather")
public class GlobalController {

    private static Logger logger = LoggerFactory.getLogger(Application.class);

    @Autowired
    private WeatherService weatherService;


    @GetMapping(value = "/today/{city}/{countryCode}")
    public ResponseEntity<Response> getWeather(
            @PathVariable String city,
            @PathVariable String countryCode
            ){
        String operation = "/api/weather/today/".concat(city).concat("/").concat(countryCode);
        try {
            Data data = weatherService.getWeather(city,countryCode);
            if (data != null) {
                Response response = ResponseBuilder.init()
                        .withMetaMethod(HttpMethod.GET)
                        .withMetaOperation(operation)
                        .addData(data)
                        .build();
                logger.info("[WEATHER-API] - weather consult success");
                return new ResponseEntity<Response>(response, HttpStatus.OK);
            }else {
                Error error = ErrorBuilder.init()
                        .addErrorCode("ERR-400")
                        .addErrorStatus(HttpStatus.BAD_REQUEST)
                        .addErrorDescription("Unknown city: ".concat(city).concat("."))
                        .build();
                Response response = ResponseBuilder.init()
                        .withMetaMethod(HttpMethod.GET)
                        .withMetaOperation(operation)
                        .addError(error)
                        .build();
                logger.error("[WEATHER-API] - not found results for "
                        .concat(city)
                        .concat(",")
                        .concat(countryCode));
                return new ResponseEntity<Response>(response, HttpStatus.BAD_REQUEST);
            }
        }catch (Exception exception){
            Error error = ErrorBuilder.init()
                    .addErrorCode("ERR-500")
                    .addErrorStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                    .addErrorDescription(exception.getMessage())
                    .build();
            Response response = ResponseBuilder.init()
                    .withMetaMethod(HttpMethod.GET)
                    .withMetaOperation(operation)
                    .addError(error)
                    .build();
          logger.error("[WEATHER-API] - Internal server error...");
            return new ResponseEntity<Response>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
