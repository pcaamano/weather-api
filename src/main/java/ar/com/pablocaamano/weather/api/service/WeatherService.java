package ar.com.pablocaamano.weather.api.service;

import ar.com.pablocaamano.weather.api.rest.Data;

public interface WeatherService {
    Data getWeather(String city, String country);
    String generateApiUrl(String city, String country);
}
