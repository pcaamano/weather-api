package ar.com.pablocaamano.weather.api.utils;

import ar.com.pablocaamano.weather.api.rest.Error;
import org.springframework.http.HttpStatus;

public class ErrorBuilder {

    Error error;

    public ErrorBuilder(){
        error = new Error();
    }

    public static ErrorBuilder init(){
        ErrorBuilder errorBuilder = new ErrorBuilder();
        return errorBuilder;
    }

    public ErrorBuilder addErrorCode(String code){
        error.setCode(code);
        return this;
    }

    public ErrorBuilder addErrorStatus(HttpStatus status){
        error.setStatus(status);
        return this;
    }

    public ErrorBuilder addErrorDescription(String description){
        error.setDescription(description);
        return this;
    }

    public Error build(){
        return this.error;
    }
}
