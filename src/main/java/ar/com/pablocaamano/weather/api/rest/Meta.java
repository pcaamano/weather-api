package ar.com.pablocaamano.weather.api.rest;

import org.springframework.http.HttpMethod;

public class Meta {
    private HttpMethod method;
    private String operation;

    public HttpMethod getMethod() {
        return method;
    }

    public void setMethod(HttpMethod method) {
        this.method = method;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }
}
