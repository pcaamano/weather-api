package ar.com.pablocaamano.weather.api.service.impl;

import ar.com.pablocaamano.weather.api.model.WeatherPrediction;
import ar.com.pablocaamano.weather.api.rest.Data;
import ar.com.pablocaamano.weather.api.service.WeatherService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class WeatherServiceImpl implements WeatherService {

    @Value("${api.key}")
    private String apiKey;

    private static final String OPEN_WEATHER_MAP_URL = "http://api.openweathermap.org/data/2.5/weather?q=";


    @Override
    public Data getWeather(String city, String country) {
        RestTemplate restTemplate = new RestTemplate();
        Data data = new Data(city.concat(",").concat(country));
        WeatherPrediction weather = restTemplate.getForObject(generateApiUrl(city,country),WeatherPrediction.class);
        data.setTemperature(String.valueOf(weather.getMain().getTemp()).concat("°C"));
        data.setDescription(weather.getWeather().get(0).getDescription());
        data.setWinds(String.valueOf(weather.getWind().getSpeed()).concat(" km/hs"));
        data.setHumidity(String.valueOf(weather.getMain().getHumidity()).concat("%"));
        return data;
    }

    @Override
    public String generateApiUrl(String city, String country) {
        return "http://api.openweathermap.org/data/2.5/weather?q="
                .concat(city)
                .concat(",%20")
                .concat(country)
                .concat("&APPID=")
                .concat(apiKey)
                .concat("&units=metric");
    }


}
