package ar.com.pablocaamano.weather.api.rest;

public class Data {
    private String city;
    private String temperature;
    private String description;
    private String winds;
    private String humidity;

    public Data(){}

    public Data(String city){
        this.city = city;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWinds() {
        return winds;
    }

    public void setWinds(String winds) {
        this.winds = winds;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }
}
