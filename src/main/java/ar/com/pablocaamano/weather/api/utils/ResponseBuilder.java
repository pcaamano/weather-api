package ar.com.pablocaamano.weather.api.utils;

import ar.com.pablocaamano.weather.api.rest.Error;
import ar.com.pablocaamano.weather.api.rest.Data;
import ar.com.pablocaamano.weather.api.rest.Meta;
import ar.com.pablocaamano.weather.api.rest.Response;
import org.springframework.http.HttpMethod;

import java.util.ArrayList;
import java.util.List;

public class ResponseBuilder {

    Response response;
    Meta meta;
    List<Data> dataList;
    List<Error> errorList;

    public ResponseBuilder(){
        response = new Response();
        meta = new Meta();
        dataList = new ArrayList<>();
        errorList = new ArrayList<>();
    }

    public static ResponseBuilder init(){
        ResponseBuilder responseBuilder = new ResponseBuilder();
        return responseBuilder;
    }

    public ResponseBuilder withMetaMethod(HttpMethod method){
        meta.setMethod(method);
        return this;
    }

    public ResponseBuilder withMetaOperation(String operation){
        meta.setOperation(operation);
        return this;
    }

    public ResponseBuilder addData(Data data){
        dataList.add(data);
        return this;
    }

    public  ResponseBuilder addError(Error error){
        errorList.add(error);
        return this;
    }

    public Response build(){
        response.setMetaData(meta);
        response.setData(dataList);
        response.setErrors(errorList);
        return this.response;
    }
}
